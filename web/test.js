var G_LOCAL_PORT = '30001';
var ws = undefined;
var ws_callback = undefined;


function connect_to_ws(){
    ws = new WebSocket("ws://127.0.0.1:" + G_LOCAL_PORT);

    ws.onopen = function(){
	console.log('ws connected');
    };
    
    ws.onclose = function(event) {
	if (event.wasClean) {
	    console.log('ws connection closed');
	} else {
	    console.log('ws disconnected'); 
	}
	console.log('ws close: ' + event.code + ' reason: ' + event.reason);
    };
    
    ws.onmessage = function (event) {
        //console.log(event.data);
        if(ws_callback){
            ws_callback(event.data);
        }
    }
}


function ws_call(info, callback){
    ws_callback = callback;
    ws.send(JSON.stringify(info));
}

/* ----------------------------------------------- */
function call_1(){
    var info = {command:'fuck'};
    ws_call(info);
}


function call_2(){
    var info = {command:'create_file', file_name:'fuck123.txt'};
    ws_call(info);
}

function call_3(){
	var app_path = document.getElementById("app_path").value;
    var info = {command:'open_app', app_path:app_path};
    ws_call(info);
}

function listScanners()
{
    var info = {command:'list_scanners'};
    ws_call(info, onScannersListReceived);
}


function onScannersListReceived(jsonString)
{
    html = "";
    obj = JSON.parse(jsonString);
    for(var key in obj) {
        html += "<option value=" + key  + ">" +obj[key] + "</option>"
    }
    document.getElementById("scanners").innerHTML = html;
}

function scan()
{
    var s = document.getElementById("scanners");
    var selectedScanner = s.options[s.selectedIndex].value;
    ws_call({command:"scan", scanner_index:selectedScanner}, onImageFileUrlReceived);
}

function onImageFileUrlReceived(url)
{
    //console.log(url);
    document.getElementById("scan_result").src=url;
}


