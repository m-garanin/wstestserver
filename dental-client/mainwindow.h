#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "appointmentinsuranceinfodialog.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_appointmentButton_clicked();

private:
    Ui::MainWindow *ui;
    AppointmentInsuranceInfoDialog* insuranceInfoDlg;
};

#endif // MAINWINDOW_H
