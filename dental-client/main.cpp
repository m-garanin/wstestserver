#include "mainwindow.h"

#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;


    int screenWidth = QApplication::desktop()->screenGeometry().width();

    w.setWindowFlags(Qt::Dialog);
    w.setGeometry(screenWidth - 300, 100, 200, 200);
    w.setFixedSize(200, 200);
    w.show();

    return a.exec();
}
