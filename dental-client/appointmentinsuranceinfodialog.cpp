#include "appointmentinsuranceinfodialog.h"
#include "ui_appointmentinsuranceinfodialog.h"

#include <QDebug>
#include <QQueue>

#define APPT_NOTES_ESTFEES "estfees"
#define APPT_NOTES_INS "ins"
#define APPT_NOTES_PH "ph"
#define APPT_NOTES_DOB "dob"
#define APPT_NOTES_ID "id"
#define APPT_NOTES_GRP "grp"
#define APPT_NOTES_RTP "rtp"
#define APPT_NOTES_XRAYS "xrays"
#define APPT_NOTES_BTHINNER "bthinner"
#define APPT_NOTES_TOOTH "tooth"
#define APPT_NOTES_NOTES "notes"

AppointmentInsuranceInfoDialog::AppointmentInsuranceInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AppointmentInsuranceInfoDialog),
    notesLoaded(false)
{
    ui->setupUi(this);
    ui->relationCombo->addItems({"Self", "Mother", "Father", "Spouse", "Other"});    
}

AppointmentInsuranceInfoDialog::~AppointmentInsuranceInfoDialog()
{
    delete ui;
}

void AppointmentInsuranceInfoDialog::onLoadNotes(const QString& notes)
{
    if (notes.size() == 0)
        return;

    //qDebug() << Q_FUNC_INFO << notes;

    ui->estFeesEdit->clear();
    ui->insuranceEdit->clear();
    ui->policyHolderEdit->clear();
    ui->dobEdit->clear();
    ui->ssnEdit->clear();
    ui->groupEdit->clear();
    ui->relationCombo->setCurrentIndex(-1);
    ui->xraysEdit->clear();
    ui->bloodThinnerCheckBox->setChecked(false);
    ui->toothEdit->clear();
    ui->notesTextEdit->clear();

    QStringList notesFields = notes.split(";", QString::SkipEmptyParts);
    foreach (QString field, notesFields) {

        QStringList keyValue = field.split(": ");

        keyValue[0] = keyValue[0].trimmed();
        keyValue[1] = keyValue[1].trimmed();


        if (QString::compare(keyValue[0], APPT_NOTES_ESTFEES, Qt::CaseInsensitive) == 0) {
            ui->estFeesEdit->setText(keyValue[1]);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_INS, Qt::CaseInsensitive) == 0) {
            ui->insuranceEdit->setText(keyValue[1]);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_PH, Qt::CaseInsensitive) == 0) {
            ui->policyHolderEdit->setText(keyValue[1]);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_DOB, Qt::CaseInsensitive) == 0) {
            ui->dobEdit->setDate(QDate::fromString(keyValue[1], "MM/dd/yyyy"));
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_ID, Qt::CaseInsensitive) == 0) {
            ui->ssnEdit->setText(keyValue[1]);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_GRP, Qt::CaseInsensitive) == 0) {
            ui->groupEdit->setText(keyValue[1]);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_RTP, Qt::CaseInsensitive) == 0) {
            int index = ui->relationCombo->findText(keyValue[1]);
            if (index >= 0)
                ui->relationCombo->setCurrentIndex(index);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_XRAYS, Qt::CaseInsensitive) == 0) {
            ui->xraysEdit->setText(keyValue[1]);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_BTHINNER, Qt::CaseInsensitive) == 0) {
            ui->bloodThinnerCheckBox->setChecked(keyValue[1] == "yes");
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_TOOTH, Qt::CaseInsensitive) == 0) {
            ui->toothEdit->setText(keyValue[1]);
            continue;
        }

        if (QString::compare(keyValue[0], APPT_NOTES_NOTES, Qt::CaseInsensitive) == 0) {
            ui->notesTextEdit->setPlainText(keyValue[1]);
            continue;
        }
    }
    notesLoaded = true;
}

void AppointmentInsuranceInfoDialog::showEvent(QShowEvent *event)
{
    QDialog::showEvent(event);
}

void AppointmentInsuranceInfoDialog::on_insertDateButton_clicked()
{
    QDateTime datetime = QDateTime::currentDateTime();
    ui->notesTextEdit->insertPlainText(datetime.toString("yyyy-MM-dd hh:mm AP") + "\n");
}

void AppointmentInsuranceInfoDialog::on_buttonBox_accepted()
{
    formNotes.clear();

    QMap<QString, QString> apptNotes;
    apptNotes[APPT_NOTES_BTHINNER] = ui->bloodThinnerCheckBox->checkState() == Qt::Checked ? "yes" : "no";
    apptNotes[APPT_NOTES_DOB] = ui->dobEdit->date().toString("MM/dd/yyyy");
    apptNotes[APPT_NOTES_ESTFEES] = ui->estFeesEdit->text();
    apptNotes[APPT_NOTES_GRP] = ui->groupEdit->text();
    apptNotes[APPT_NOTES_ID] = ui->ssnEdit->text();
    apptNotes[APPT_NOTES_INS] = ui->insuranceEdit->text();
    apptNotes[APPT_NOTES_NOTES] = ui->notesTextEdit->toPlainText();
    apptNotes[APPT_NOTES_PH] = ui->policyHolderEdit->text();
    apptNotes[APPT_NOTES_RTP] = ui->relationCombo->currentText();
    apptNotes[APPT_NOTES_TOOTH] = ui->toothEdit->text();
    apptNotes[APPT_NOTES_XRAYS] = ui->xraysEdit->text();

    foreach (const QString &key, apptNotes.keys())
        formNotes.append(key + ": " + apptNotes[key] + ";");
}
