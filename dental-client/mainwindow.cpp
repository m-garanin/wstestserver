#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "appointmentinsuranceinfodialog.h"

#include <QDesktopWidget>
#include <windows.h>
#include <QMessageBox>
#include <QThread>

struct WinOmsCapture {
    QList<QString> captions;
    AppointmentInsuranceInfoDialog* dlg;
    HWND notesEditHwnd = 0;
};


BOOL CALLBACK EnumWindowsCallback(HWND hwnd, LPARAM lParam)
{
    WinOmsCapture *capture = (WinOmsCapture*)lParam;

    wchar_t caption[255];
    GetWindowText(hwnd, (LPWSTR)caption, 255);
    QString captionStr = QString::fromWCharArray(caption);

    if (capture->captions.empty()) {
        wchar_t className[255];

        WINDOWINFO winInfo;
        GetWindowInfo(hwnd, &winInfo);
        GetClassName(hwnd, (LPWSTR)className, 255);

        //qDebug() << Q_FUNC_INFO << hwnd << QString::fromWCharArray(className) << winInfo.dwStyle;

        if (QString::fromWCharArray(className) == "Edit" && winInfo.dwStyle == 1176502340) {
            PostMessage(capture->notesEditHwnd, WM_KEYDOWN, 0x30, 0);
            QThread::currentThread()->msleep(100);
            PostMessage(capture->notesEditHwnd, WM_KEYDOWN, VK_BACK, 0);
            QThread::currentThread()->msleep(100);

            wchar_t szBuf[2048];
            memset(&szBuf, 0, sizeof(2048));
            SendMessage(hwnd, WM_GETTEXT, sizeof(szBuf) / sizeof(szBuf[0]), (LPARAM)szBuf);
            capture->notesEditHwnd = hwnd;
            capture->dlg->onLoadNotes(QString::fromWCharArray(szBuf));
            return false;
        }

    } else if (captionStr.startsWith(capture->captions.front())) {
        //qDebug() << Q_FUNC_INFO << "Found: " << capture->captions.front();
        capture->captions.pop_front();
        EnumChildWindows(hwnd, EnumWindowsCallback, (LPARAM)capture);
        return false;
    }
    return true;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    insuranceInfoDlg = new AppointmentInsuranceInfoDialog(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_appointmentButton_clicked()
{
    insuranceInfoDlg->notesLoaded = false;

    WinOmsCapture capture;
    capture.captions = {"CS WinOMS Software:", "Appointment Data Entry", "Appointment"};
    capture.dlg = insuranceInfoDlg;

    EnumWindows(EnumWindowsCallback, (LPARAM)&capture);

    if (insuranceInfoDlg->notesLoaded) {
        QPoint screenCenter = QApplication::desktop()->screenGeometry().center();
        insuranceInfoDlg->setGeometry(screenCenter.x() - insuranceInfoDlg->width() / 2.0, screenCenter.y() - insuranceInfoDlg->height() / 2.0, insuranceInfoDlg->width(), insuranceInfoDlg->height());
        if (insuranceInfoDlg->exec() == QDialog::Accepted) {
            wchar_t szBuf[2048];

            memset(szBuf, 0, 2048);
            SendMessage(capture.notesEditHwnd, WM_SETTEXT, 0, (LPARAM)szBuf);

            insuranceInfoDlg->formNotes.toWCharArray(szBuf);
            //if (!SetWindowText(capture.notesEditHwnd, szBuf))
            //    QMessageBox::critical(this, "Notes saving error", "Cannot set notes in WinOMS");
            if (!SendMessage(capture.notesEditHwnd, WM_SETTEXT, 0, (LPARAM)szBuf))
                QMessageBox::critical(this, "Notes saving error", "Cannot set notes in WinOMS");

            PostMessage(capture.notesEditHwnd, WM_KEYDOWN, 0x30, 0);
            QThread::currentThread()->msleep(100);
            PostMessage(capture.notesEditHwnd, WM_KEYDOWN, VK_BACK, 0);
            QThread::currentThread()->msleep(100);

            //UpdateWindow(capture.notesEditHwnd);

        }
    } else
        QMessageBox::critical(this, "Notes loading error", "Cannot load notes. Open appointment window in WinOms and set focus on notes editor");
}
