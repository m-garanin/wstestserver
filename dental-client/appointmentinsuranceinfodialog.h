#ifndef APPOINTMENTINSURANCEINFODIALOG_H
#define APPOINTMENTINSURANCEINFODIALOG_H

#include <QDialog>

namespace Ui {
class AppointmentInsuranceInfoDialog;
}

class AppointmentInsuranceInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AppointmentInsuranceInfoDialog(QWidget *parent = 0);
    ~AppointmentInsuranceInfoDialog();

    void onLoadNotes(const QString& notes);

    bool notesLoaded;
    QString formNotes;

protected:
    virtual void showEvent(QShowEvent *event);

private slots:
    void on_insertDateButton_clicked();

    void on_buttonBox_accepted();

private:
    Ui::AppointmentInsuranceInfoDialog *ui;    
};

#endif // APPOINTMENTINSURANCEINFODIALOG_H
