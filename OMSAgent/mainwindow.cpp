#pragma comment (lib, "User32.lib")

#include "windows.h"


#include "mainwindow.h"
#include "ui_mainwindow.h"


BOOL CALLBACK myEnumWnd(HWND hwnd, LPARAM lParam)
{
     DWORD dwProcessID;
     DWORD zP = lParam;
     GetWindowThreadProcessId(hwnd, &dwProcessID);
     if (zP == dwProcessID) {
         // We have found a top level window of the
         // process we've started.
         // Now do something with it.
         qDebug() << "FIND WINDOW:" << hwnd;
     }else{
         //qDebug() << "SKIP WINDOW:" << dwProcessID << zP;
     }
     return TRUE;
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_server = new WSServer(this);
    connect(m_server, SIGNAL(newLog(QString)), this, SLOT(onLog(QString)) );

    m_server->start(30001);

    setWindowFlags(Qt::Window
                   //| Qt::CustomizeWindowHint |Qt::WindowTitleHint
                   //| Qt::WindowMaximizeButtonHint
                   //|Qt::WindowMinMaxButtonsHint
                   //|Qt::WindowFullscreenButtonHint
                   | Qt::WindowStaysOnTopHint);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onLog(QString msg)
{
    ui->txtLog->append(msg);
}

void MainWindow::on_pushButton_clicked()
{
    pr = new QProcess;
    connect(pr, &QProcess::started, [=](){
        HWND hw;
        while(true){
            //qDebug() << "ZZ:" << pr->processId() << pr->pid()->dwThreadId << pr->pid()->dwProcessId;
            //qDebug() << "STATE:" << pr->state();
            EnumWindows(myEnumWnd, pr->pid()->dwProcessId);
            hw = FindWindow(NULL, L"Untitled - Notepad");
            if(hw)
                break;
            QThread::sleep(1);
        }
        qDebug() << "HWND 33:" << hw;
        BringWindowToTop(hw);
        SetForegroundWindow(hw);
    });

    QString fname = "C:\\Windows\\notepad.exe";
    //fname = "\"C:\\Program Files (x86)\\WinBroadcaster\\WinBroadcaster.exe\"";
    qDebug() << fname;
    pr->start(fname);
    //pr->waitForStarted();


}

void MainWindow::on_pushButton_2_clicked()
{
    HWND hw;
    hw = FindWindow(NULL, L"Untitled - Notepad");
    qDebug() << "HWND 22:" << hw;

   EnumWindows(myEnumWnd, pr->pid()->dwProcessId);
}
