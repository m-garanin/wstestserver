#ifndef WSSERVER_H
#define WSSERVER_H

#include "twain/TwainAppCMD.h"

#include <QObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtWebSockets/QtWebSockets>

class WSServer : public QObject
{
    Q_OBJECT
public:
    explicit WSServer(QObject *parent = 0);
    ~WSServer();

    static TwainAppCMD  *twainApplicationCMD;

    bool start(int port);

signals:
    void newLog(QString msg);

public slots:
    void onConnect();
    void onDisconnect();
    void processTextMessage(QString message);

private:
    QUrl EnableScanDS();

    QWebSocketServer* m_server;    

    void log(QString msg);
};

#endif // WSSERVER_H
