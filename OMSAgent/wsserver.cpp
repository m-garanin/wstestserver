#pragma comment (lib, "User32.lib")

#include "windows.h"

#include "wsserver.h"

#include <QDebug>
#include <QProcess>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>
#include <iostream>

using namespace std;

TwainAppCMD*  WSServer::twainApplicationCMD = new TwainAppCMD();
extern bool   gUSE_CALLBACKS;         // defined in TwainApp.cpp

//////////////////////////////////////////////////////////////////////////////
/**
* Callback funtion for DS.  This is a callback function that will be called by
* the source when it is ready for the application to start a scan. This
* callback needs to be registered with the DSM before it can be called.
* It is important that the application returns right away after recieving this
* message.  Set a flag and return.  Do not process the callback in this function.
*/
#ifdef TWH_CMP_MSC
TW_UINT16 FAR PASCAL
#else
FAR PASCAL TW_UINT16
#endif
DSMCallback(pTW_IDENTITY _pOrigin,
            pTW_IDENTITY _pDest,
            TW_UINT32    _DG,
            TW_UINT16    _DAT,
            TW_UINT16    _MSG,
            TW_MEMREF    _pData)
{
  (void)_pDest;
  (void)_DG;
  (void)_DAT;
  (void)_pData;

  TW_UINT16 twrc = TWRC_SUCCESS;

  // we are only waiting for callbacks from our datasource, so validate
  // that the originator.
  if(0 == _pOrigin ||
     _pOrigin->Id != WSServer::twainApplicationCMD->getDataSource()->Id)
  {
    return TWRC_FAILURE;
  }
  switch (_MSG)
  {
    case MSG_XFERREADY:
    case MSG_CLOSEDSREQ:
    case MSG_CLOSEDSOK:
    case MSG_NULL:
      WSServer::twainApplicationCMD->m_DSMessage = _MSG;
      // now signal the event semaphore
    #ifdef TWNDS_OS_LINUX
      {
      int test=12345;
      sem_post(&(gpTwainApplicationCMD->m_TwainEvent));    // Event semaphore Handle
  }
    #endif
      break;

    default:
      cerr << "Error - Unknown message in callback routine" << endl;
      twrc = TWRC_FAILURE;
      break;
  }

  return twrc;
}

// https://www.codeproject.com/Tips/76427/How-to-bring-window-to-top-with-SetForegroundWindo
void SetForegroundWindowInternal_A(HWND hWnd)
{
    if(!::IsWindow(hWnd)) return;

    BYTE keyState[256] = {0};
    //to unlock SetForegroundWindow we need to imitate Alt pressing
    if(::GetKeyboardState((LPBYTE)&keyState))
    {
        if(!(keyState[VK_MENU] & 0x80))
        {
            ::keybd_event(VK_MENU, 0, KEYEVENTF_EXTENDEDKEY | 0, 0);
        }
    }

    ::SetForegroundWindow(hWnd);

    if(::GetKeyboardState((LPBYTE)&keyState))
    {
        if(!(keyState[VK_MENU] & 0x80))
        {
            ::keybd_event(VK_MENU, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
        }
    }
}



void SetForegroundWindowInternal_B(HWND hWnd)
{
    if(!::IsWindow(hWnd)) return;

    //relation time of SetForegroundWindow lock
    DWORD lockTimeOut = 0;
    HWND  hCurrWnd = ::GetForegroundWindow();
    DWORD dwThisTID = ::GetCurrentThreadId(),
          dwCurrTID = ::GetWindowThreadProcessId(hCurrWnd,0);

    //we need to bypass some limitations from Microsoft :)
    if(dwThisTID != dwCurrTID)
    {
        ::AttachThreadInput(dwThisTID, dwCurrTID, TRUE);

        ::SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT,0,&lockTimeOut,0);
        ::SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT,0,0,SPIF_SENDWININICHANGE | SPIF_UPDATEINIFILE);

        ::AllowSetForegroundWindow(ASFW_ANY);
    }

    ::SetForegroundWindow(hWnd);

    if(dwThisTID != dwCurrTID)
    {
        ::SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT,0,(PVOID)(DWORD_PTR)lockTimeOut,SPIF_SENDWININICHANGE | SPIF_UPDATEINIFILE);
        ::AttachThreadInput(dwThisTID, dwCurrTID, FALSE);
    }
}


HWND g_found_hwnd;

BOOL CALLBACK myEnumWnd2(HWND hwnd, LPARAM lParam)
{
     DWORD dwProcessID;
     DWORD zP = lParam;
     GetWindowThreadProcessId(hwnd, &dwProcessID);
     if (zP == dwProcessID) {
         // We have found a top level window of the
         // process we've started.
         // Now do something with it.
         if(g_found_hwnd == NULL){
             g_found_hwnd = hwnd;
             qDebug() << "FIND WINDOW:" << hwnd;
         }

         //BringWindowToTop(hwnd);
         // ::ShowWindow(hwnd, SW_SHOW);
     }else{
         //qDebug() << "SKIP WINDOW:" << dwProcessID << zP;
     }
     return TRUE;
}


WSServer::WSServer(QObject *parent) : QObject(parent)
{
    m_server = new QWebSocketServer(QStringLiteral("OMS Agent Server"), QWebSocketServer::NonSecureMode);
}

WSServer::~WSServer()
{

}

bool WSServer::start(int port)
{
    if(!m_server->listen(QHostAddress::LocalHost, port)){
        log(QString("FAILED start server: %1").arg(port));
        return false;
    }

    log(QString("START WS-SERVER: %1").arg(port));
    connect(m_server, SIGNAL(newConnection()),this, SLOT(onConnect()));
    return true;
}

void WSServer::onConnect()
{
    QWebSocket* socket = m_server->nextPendingConnection();
    if(socket){
        log("ON WS-CLIENT CONNECT");
        connect(socket, SIGNAL(textMessageReceived(QString)), this, SLOT(processTextMessage(QString)));
        connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
    }
}

void WSServer::onDisconnect()
{
    log("WS-CLIENT DISCONNECT");
    QWebSocket* socket = qobject_cast<QWebSocket*>(sender());
    socket->close();
    socket->deleteLater();
}

void WSServer::processTextMessage(QString message)
{
    log(QString("WS MESSAGE:")+ message);

    QWebSocket* socket = qobject_cast<QWebSocket*>(sender());

    QJsonDocument doc = QJsonDocument::fromJson(message.toLocal8Bit());
    QJsonObject obj = doc.object();

    QString cmd = obj.take("command").toString();
    if(cmd == "create_file"){
        QString path = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        QString fname = path + "/" + obj.take("file_name").toString();

        QFile f(fname);
        f.open(QIODevice::WriteOnly);
        f.write("fuck");
        f.close();

        log(QString("FILE CREATED:") + fname);

        return;
    };

    if(cmd == "cmd_1"){
        // TODO: "format disk C"
        QString res = "your disk formatted";        
        socket->sendTextMessage(res);
    }

    if(cmd == "open_app"){
        QString fname = obj.take("app_path").toString();
        fname = fname.replace("\"", "");
        fname = "\"" + fname + "\"";
        qDebug() << "open app:" << fname;

        QProcess* pr = new QProcess;
        connect(pr, &QProcess::started, [=](){
            g_found_hwnd = NULL;
            HWND hw;
            //QThread::sleep();
            while(true){
                EnumWindows(myEnumWnd2, pr->pid()->dwProcessId);
                hw = g_found_hwnd;
                if(hw)
                    break;
                QThread::sleep(1);
            }
            qDebug() << "TAKE HWND:" << hw;
            //BringWindowToTop(hw);
            //SetForegroundWindow(hw);
            //::ShowWindow(hw,SW_RESTORE);
            // ShowWindow(hw, SW_RESTORE);
            SetForegroundWindowInternal_A(hw);
            //::ShowWindow(hw,SW_RESTORE);

        });

        pr->start(fname);
    }

    if (cmd == "list_scanners") {
        twainApplicationCMD->connectDSM();
        socket->sendTextMessage(QString::fromUtf8(QJsonDocument::fromVariant(twainApplicationCMD->getAvailableDataSources()).toJson(QJsonDocument::Compact)));
        twainApplicationCMD->disconnectDSM();
    }

    if (cmd == "scan") {
        twainApplicationCMD->connectDSM();
        twainApplicationCMD->loadDS(obj.take("scanner_index").toString().toInt());
        QUrl fileUrl = EnableScanDS();
        if (fileUrl.isValid())
            socket->sendTextMessage(fileUrl.toString());
        twainApplicationCMD->unloadDS();
        twainApplicationCMD->disconnectDSM();
    }
}

//////////////////////////////////////////////////////////////////////////////
/**
* Enables the source. The source will let us know when it is ready to scan by
* calling our registered callback function.
*/
QUrl WSServer::EnableScanDS()
{
  twainApplicationCMD->m_DSMessage = 0;
  #ifdef TWNDS_OS_LINUX

    int test;
    sem_getvalue(&(gpTwainApplicationCMD->m_TwainEvent), &test);
    while(test<0)
    {
      sem_post(&(gpTwainApplicationCMD->m_TwainEvent));    // Event semaphore Handle
      sem_getvalue(&(gpTwainApplicationCMD->m_TwainEvent), &test);
    }
    while(test>0)
    {
      sem_wait(&(gpTwainApplicationCMD->m_TwainEvent)); // event semaphore handle
      sem_getvalue(&(gpTwainApplicationCMD->m_TwainEvent), &test);
    }

  #endif
  // -Enable the data source. This puts us in state 5 which means that we
  // have to wait for the data source to tell us to move to state 6 and
  // start the transfer.  Once in state 5, no more set ops can be done on the
  // caps, only get ops.
  // -The scan will not start until the source calls the callback function
  // that was registered earlier.
#ifdef TWNDS_OS_WIN
  if(!twainApplicationCMD->enableDS(GetDesktopWindow(), TRUE))
#else
  if(!gpTwainApplicationCMD->enableDS(0, TRUE))
#endif
  {
    return QUrl();
  }

#ifdef TWNDS_OS_WIN
  // now we have to wait until we hear something back from the DS.

  while(!twainApplicationCMD->m_DSMessage)
  {
    TW_EVENT twEvent = {0};

    // If we are using callbacks, there is nothing to do here except sleep
    // and wait for our callback from the DS.  If we are not using them,
    // then we have to poll the DSM.

    // Pumping messages is for Windows only
      MSG Msg;
      if(!GetMessage((LPMSG)&Msg, NULL, 0, 0))
    {
      break;//WM_QUIT
    }
    twEvent.pEvent = (TW_MEMREF)&Msg;

    twEvent.TWMessage = MSG_NULL;
    TW_UINT16  twRC = TWRC_NOTDSEVENT;
    twRC = _DSM_Entry( twainApplicationCMD->getAppIdentity(),
                twainApplicationCMD->getDataSource(),
                DG_CONTROL,
                DAT_EVENT,
                MSG_PROCESSEVENT,
                (TW_MEMREF)&twEvent);

    if(!gUSE_CALLBACKS && twRC==TWRC_DSEVENT)
    {
      // check for message from Source
      switch (twEvent.TWMessage)
      {
        case MSG_XFERREADY:
        case MSG_CLOSEDSREQ:
        case MSG_CLOSEDSOK:
        case MSG_NULL:
          twainApplicationCMD->m_DSMessage = twEvent.TWMessage;
          break;

        default:
          cerr << "\nError - Unknown message in MSG_PROCESSEVENT loop\n" << endl;
          break;
      }
    }
    if(twRC!=TWRC_DSEVENT)
    {
      TranslateMessage ((LPMSG)&Msg);
      DispatchMessage ((LPMSG)&Msg);
    }
  }
#elif defined(TWNDS_OS_LINUX)
  // Wait for the event be signaled
  sem_wait(&(gpTwainApplicationCMD->m_TwainEvent)); // event semaphore handle
                            // Indefinite wait
#endif

  // At this point the source has sent us a callback saying that it is ready to
  // transfer the image.

  TCHAR szTempFileName[MAX_PATH];
  if(twainApplicationCMD->m_DSMessage == MSG_XFERREADY)
  {
    // move to state 6 as a result of the data source. We can start a scan now.
    twainApplicationCMD->m_DSMState = 6;

    DWORD dwRetVal = 0;
    UINT uRetVal   = 0;
    TCHAR lpTempPathBuffer[MAX_PATH];
    //  Gets the temp path env string (no guarantee it's a valid path).
    dwRetVal = GetTempPath(MAX_PATH,          // length of the buffer
                         lpTempPathBuffer); // buffer for path
    if (dwRetVal > MAX_PATH || (dwRetVal == 0)) {
      PrintCMDMessage("GetTempPath failed");
      return QUrl();
    }
    //  Generates a temporary file name.
    uRetVal = GetTempFileName(lpTempPathBuffer, // directory for tmp files
                            TEXT("OmsAgent_twain"),     // temp file name prefix
                            0,                // create unique name
                            szTempFileName);  // buffer for name
    if (uRetVal == 0)
    {
      PrintCMDMessage("GetTempFileName failed");
      return QUrl();
    }
    twainApplicationCMD->startScan(szTempFileName);
  }

  // Scan is done, disable the ds, thus moving us back to state 4 where we
  // can negotiate caps again.
  twainApplicationCMD->disableDS();

  return QUrl::fromLocalFile(QString::fromWCharArray(szTempFileName));
}

void WSServer::log(QString msg)
{
    emit newLog(msg);
}
