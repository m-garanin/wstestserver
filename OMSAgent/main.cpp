#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qApp->setStyleSheet("MainWindow { background-color: #1A192E;}");

    MainWindow w;
    w.show();

    return a.exec();
}
