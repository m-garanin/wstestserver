#-------------------------------------------------
#
# Project created by QtCreator 2017-10-06T00:41:33
#
#-------------------------------------------------

QT       += core gui websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OMSAgent
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    wsserver.cpp \
    twain/TwainApp.cpp \
    twain/TwainAppCMD.cpp \
    main.cpp \
    mainwindow.cpp \
    wsserver.cpp \
    twain/CommonTWAIN.cpp \
    twain/CTiffWriter.cpp \
    twain/DSMInterface.cpp \
    twain/TwainApp.cpp \
    twain/TwainAppCMD.cpp \
    twain/TwainString.cpp

HEADERS  += mainwindow.h \
    wsserver.h \
    twain/TwainApp.h \
    twain/TwainAppCMD.h \
    mainwindow.h \
    wsserver.h \
    twain/Common.h \
    twain/CommonTWAIN.h \
    twain/CTiffWriter.h \
    twain/DSMInterface.h \
    twain/FreeImage.h \
    twain/twain.h \
    twain/TwainApp.h \
    twain/TwainAppCMD.h \
    twain/TwainString.h

FORMS    += mainwindow.ui

LIBS += -L$$PWD/twain/libs -lfreeimagex64
#LIBS += -lKernel32 -lUser32
