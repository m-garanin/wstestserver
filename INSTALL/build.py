import os, os.path, shutil
import subprocess

BUILD_PATH = 'C:/DEV/wstestserver/build-OMSAgent-Desktop_Qt_5_9_1_MSVC2017_64bit-Release/release/'
WINDEPLOY = "C:/Qt/5.9.1/msvc2017_64/bin/windeployqt.exe"

OUT_FOLDER = 'FILES'
APP_NAME = 'OMSAgent.exe'


def start():
	if os.path.exists(OUT_FOLDER):
		shutil.rmtree(OUT_FOLDER)
		
	os.mkdir(OUT_FOLDER)
	copy_app()
	copy_vc_libs()
	run_windeploy()
	
	
def copy_vc_libs():
	fpath = "LIBS_64"
	for item in os.listdir(fpath):
		shutil.copy(os.path.join(fpath, item), OUT_FOLDER)
	
	
def copy_app():
	# copy app and resources
	shutil.copy(os.path.join(BUILD_PATH, APP_NAME), OUT_FOLDER)
	
	
def run_windeploy():
	args = ["--no-translations", "--no-system-d3d-compiler", 
			"-no-quick-import", "--no-angle", 
			"--no-opengl-sw"
			]
	subprocess.call([WINDEPLOY, os.path.join(OUT_FOLDER, APP_NAME)] + args)
	
	
start()
