!define VERSION "0.2"
Name "OMSAgent ${VERSION}"
OutFile "OMSAgent-setup-${VERSION}.exe"

!include "MUI2.nsh"

SetCompressor lzma
XPStyle on

; The default installation directory
InstallDir "$PROGRAMFILES\OMSAgent"

;Get installation folder from registry if available
InstallDirRegKey HKCU "Software\OMSAgent" ""

; Request application privileges for Windows Vista
RequestExecutionLevel highest

Var StartMenuFolder

;--------------------------------
;Pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY

;Start Menu Folder Page Configuration
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "OMSAgent"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\OMSAgent"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder

!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_RUN "$INSTDIR\OMSAgent.exe"
!insertmacro MUI_PAGE_FINISH

; uninstaller
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"



Section "Main Section"
  SetOutPath $INSTDIR
  File /r FILES\*.*
  
  # Desktop shortcut
  CreateShortCut "$DESKTOP\OMSAgent.lnk" "$INSTDIR\OMSAgent.exe"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
        ;Create shortcuts
                CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
                CreateShortCut "$SMPROGRAMS\$StartMenuFolder\WinBroadcaster.lnk" "$INSTDIR\OMSAgent.exe"
                CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  !insertmacro MUI_STARTMENU_WRITE_END

  ;Store installation folder
  WriteRegStr HKCU "Software\OMSAgent" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

 SectionEnd


 Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\Uninstall.exe"

  RMDir /r "$INSTDIR"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder

  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  RMDir /r "$SMPROGRAMS\$StartMenuFolder"

  DeleteRegKey /ifempty HKCU "Software\WinBroadcaster"

SectionEnd
